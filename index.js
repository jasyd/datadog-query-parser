console.log('Hello from datadog chrome extension')

const sendToClipboard = (value) => {
  var textCopy = value // Text to Copy
  var copyFrom = document.createElement("textarea");

  copyFrom.textContent = `${textCopy}`;
  document.body.appendChild(copyFrom);

  copyFrom.select();
  document.execCommand(`copy`);

  copyFrom.blur();
  document.body.removeChild(copyFrom);
}

function getQuery() {
  const className = 'druids_typography_code__code';
  const element = document.getElementsByClassName(className)[0]
  const logText = element.innerText.replace('\t', '').replaceAll('\n', '');
  const json = logText.slice(logText.indexOf('{'), logText.length);
  const obj = JSON.parse(json)

  const query = obj.query.replace('\\"', '');

  const queryWithParams = query.replace(/\$\d+/g, (match) => {
    const index = parseInt(match.slice(1)) - 1;
    return `'${obj.parameters[index]}'`;
  });

  sendToClipboard(queryWithParams);
};

chrome.runtime.onMessage.addListener(function(msg, sender, sendResponse) {
    if (msg.type === 'copyQuery') {
        getQuery();
    }
});
