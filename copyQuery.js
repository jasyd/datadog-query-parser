const contextMenuItem = {
  id: "copyQuery",
  title: "Copy TypeORM query"
}

chrome.contextMenus.create(contextMenuItem)

chrome.contextMenus.onClicked.addListener(async () => {
    chrome.tabs.query({'active': true, 'lastFocusedWindow': true }, async function (tabs) {
      await chrome.tabs.sendMessage(tabs[0].id, { type: "copyQuery" });
    });
})

